#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/list.h>

#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "device_tp3"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

//----------------------------------------------------------------

typedef struct {
    char *data;
    int size,cursor;
    struct list_head list;
} data_node_t;

struct list_head ma_list;

int init_flag = 0;

//---------------------------------------------------------------

static void list_destroy(void){
    data_node_t *pos, *next;
    printk(KERN_ALERT "[ Driver action ] free data buffer state \n");
    list_for_each_entry_safe(pos,next,&ma_list,list) {
        list_del(&pos->list);
        kfree(pos->data);
        kfree(pos);
    }
}

//----------------------------------------------------------------

ssize_t my_read(struct file *f,char *buf, size_t s, loff_t *t){
    data_node_t *dnt;
    int size_to_copy, data_in_node;
    printk(KERN_ALERT "[ Driver action ] read of %ld octet \n",s);
    if(s > 0){
        if(!list_empty(&ma_list)){
            dnt = list_first_entry(&ma_list,data_node_t,list);
            data_in_node = dnt->size - dnt->cursor;
            size_to_copy = MIN(s,data_in_node);
            if(copy_to_user(buf,dnt->data + dnt->cursor,size_to_copy) == 0){
                dnt->cursor = dnt->cursor + size_to_copy;
                if(dnt->cursor >= dnt->size){
                    list_del(&dnt->list);
                    kfree(dnt->data);
                    kfree(dnt);
                }
                return size_to_copy;
            } else {
                return -EFAULT;
            }
        } 
    }
    return 0;
}
ssize_t my_write(struct file *f,const char *buf, size_t s, loff_t *t){
    printk(KERN_ALERT "[ Driver action ] write of %ld octet \n",s);
    if(s > 0){
        data_node_t *dnt = kmalloc(sizeof(data_node_t),GFP_KERNEL);
        if(dnt){
            dnt->data = kmalloc(sizeof(char)*s,GFP_KERNEL);
            if(dnt->data){
                dnt->size = s - copy_from_user(dnt->data,buf,s);;
                dnt->cursor = 0;
                INIT_LIST_HEAD(&(dnt->list));
                list_add_tail(&(dnt->list),&ma_list);
                return dnt->size;
            }
            kfree(dnt);
        } 
        return -EINVAL;
    }
    return s;
}
int my_open(struct inode *i, struct file *f){
    if((f->f_flags & O_ACCMODE) == O_WRONLY){
        list_destroy();
    }
    return 0;
}
int my_release(struct inode *i,struct file *f){
    return 0;
}

//---------------------------------------------------------------------

static dev_t dev;

static struct cdev *my_cdev;

static struct file_operations my_fops = {
    .owner = THIS_MODULE,
    .read = my_read,
    .write = my_write,
    .open = my_open,
    .release = my_release,
};

static struct class *cl;

//----------------------------------------------------------------------

static int buff_init(void){

      if( alloc_chrdev_region(&dev,0,1,"sample") == -1 )
    {
        printk(KERN_ALERT">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }
    printk(KERN_ALERT "[ Init allocated ] (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));
    printk(KERN_ALERT "[ Driver State ] starting\n");

    my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;
    my_cdev->owner = THIS_MODULE;

    cl = class_create( THIS_MODULE, "chardev" );

    if ( cl == NULL )
    {
        printk( KERN_ALERT ">>> ERROR : Class creation failed\n" );
        unregister_chrdev_region( dev, 1 );
        return -EINVAL;
    }

     if( device_create( cl, NULL, dev, NULL, "sample" ) == NULL )
    {
        printk( KERN_ALERT ">>> ERROR : Device creation failed\n" );
        class_destroy(cl);
        unregister_chrdev_region( dev, 1 );
        return -EINVAL;
    }

    cdev_add(my_cdev,dev,1);  

    INIT_LIST_HEAD(&ma_list);

    return 0;

}
static void buff_cleanup(void){
    printk(KERN_ALERT "[ Driver State ] stopping\n");
    cdev_del(my_cdev);
    device_destroy( cl, dev );
    class_destroy( cl );
    unregister_chrdev_region(dev,1);
    list_destroy();
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(buff_init);
module_exit(buff_cleanup);

